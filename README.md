# myImg

#### 介绍
图床仓库



#### 使用说明

1.  图床软件采用PicGo，下载地址：https://github.com/Molunerfinn/PicGo/releases
2.  安装PicGo，插件搜索下载Gitee-upload,这个需要note.js支持
3.  安装note.js  下载地址：https://nodejs.org/en/
4.  在gitee中创建一个仓库，并且设置为公有仓库。
5.  在gitee个人中心中生成一个私人令牌，需要PicGo执行commit，pull等操作
6.  在PicGo -> 图床设置 ->  gitee ；设置配置好的仓库路径，分支和令牌等等
7.  在设置中配置上传时修改文件名为时间戳（可选操作）

##### 对Typora笔记的集成

在 文件 -> 偏好设置 -> 图像 ; 
1. 插入图片时选择 上传图片，对本地和网络图片都勾选即可
2. 上传服务选择PicGo(app)，配置安装路径就可以了

##### 访问实例
1. 生成时链接格式选择markdown:

```
  ![](https://gitee.com/zhao_shu_chao/img/raw/master/blog/20210522184639.jpeg)
```


2. 生成时链接选择URL：

```
  https://gitee.com/zhao_shu_chao/img/raw/master/blog/20210522184639.jpeg
```


3. 生成时链接选择HTML：

```
  <img src="https://gitee.com/zhao_shu_chao/img/raw/master/blog/20210522184639.jpeg"/>
```

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


